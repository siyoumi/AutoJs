requestScreenCapture(); //截图
setScreenMetrics(1080, 1920);

var func = require('./common/func.js');
var KEY__boss_pre = 'boss_pre'; //上一次发送内容【判断用】
var KEY__hs_send_list = 'hs_send_list'; //已发送列表【判断用】
//
var KEY__next = 'next'; //将要发送码
var KEY__next_desc = 'next_desc'; //将要发送介绍
//
var SLEEP__s = 60 * 20;
//编号
var Numder = '';

main();
function main() {
    //
    // funcGetHSRes();
    // funcGetHSRes_GetCard();
    // exit();
    //
    var Numder = '';
    while (true) {
        Numder = rawInput("请输入编号", "");
        if (Numder != '') {
            break;
        }
    }
    //
    var i = 0;
    while (i < 100) {
        var file_path = "/sdcard/Pictures/autojs/txt/lock.txt";
        var numder_lock = files.read(file_path);
        if (numder_lock != '' && numder_lock != Numder) {
            func.funcAlert('有其他编号正式工作，等2分钟再试试');
            func.funcSleep(120);
            continue;
        }
        func.funcAlert('锁编号：' + Numder);
        files.write(file_path, Numder);

        //清理环境
        funcClear();
        //
        var get_ok = funcGetBossRes();
        // var get_ok = false;
        if (!get_ok) {
            home();
            func.funcAlert('获取大姐大素材失败，准备获取花生素材');
            func.funcSleep(2);
            var is_ok = funcGetHSRes();
            funcBackN(3);
            home();
            if (!is_ok) {
                func.funcAlert('获取朋友圈素材失败，10秒后继续');
                func.funcSleep(10);
                continue;
            }
        }
        //
        funcWxQuanSendImg()
        //
        //不返回n次微信会异常，点击去别人朋友圈，永远去自己的
        funcBackN(15);
        //
        func.funcAlert('解锁编号');
        files.write(file_path, '');
        //
        func.funcAlert('休息一下');
        func.funcSleep(SLEEP__s);
        //
        i++;
    }
    //////////////////////////////
    func.funcAlert('结束脚本');
    func.funcSleep(3);
    exit();
}

/** 清理环境 */
function funcClear() {
    func.funcAlert('先清理下一次发送内容')
    var app = func.funcStorageObj();
    app.remove(KEY__next);
    app.remove(KEY__next_desc);
    //
    func.funcAlert('清空粘贴板')
    func.funcSetCp('');
}

/**
 * 获取花生日记朋友圈素材
 */
function funcGetHSRes() {
    funcHsOpen();
    func.funcSleep(3);
    //
    if (id("pic_iv").exists()) {
        func.funcAlert('首页存在广告，准备关闭');
        func.funcSleep(2);
        id("close_iv").findOne(2000).click();
    }
    func.funcSleep(1);
    func.funcAlert('点击猜你喜欢');
    className("android.widget.TextView").text("猜你喜欢").findOne(2000).parent().click()
    func.funcSleep(2);
    funcGetHSRes_GetCard();
    func.funcSleep(3);
    //
    func.funcAlert('开始分享');
    var btn_share = text("分享").findOne(2000);
    if (!btn_share) {
        func.funcAlert('异常：分享按钮找不到');
        func.funcSleep(3);
        return false;
    }
    btn_share.click();
    func.funcSleep(3);
    func.funcAlert('取消标题');
    var btn_title = id("show_title_tv").findOne(2000);
    if (btn_title.selected()) {
        btn_title.click();
    }
    func.funcSleep(3);
    func.funcAlert('取消大图');
    var btn_big_pic = id("fragment_share_selectIv").findOne(2000);
    if (btn_big_pic == null) {
        func.funcAlert('分享页异常');
        return false;
    }
    if (btn_big_pic.selected()) {
        btn_big_pic.click();
    }
    func.funcSleep(2);
    func.funcAlert('小图全部选择');
    id("fragment_share_picGv").findOne().children().forEach(child => {
        var target = child.findOne(id("view_share_pic_item_selectIv"));
        if (!target.selected()) {
            target.click();
        }
    });
    func.funcSleep(2);
    func.funcAlert('点击复制方案按钮');
    id("fragment_share_copyTv").findOne(2000).click()
    func.funcSleep(2);
    var txt = func.funcGetCp();
    func.funcSleep(2);
    func.funcAlert("保存朋友圈内容");
    func.funcStorageSet(KEY__next, txt);
    //
    func.funcAlert("开始保存图片");
    id("fragment_share_sharePicTv").findOne(2000).click()
    func.funcSleep(1);
    click('微信朋友圈');
    func.funcSleep(3);

    return true;
}

/** 点前往领券 */
function funcGetHSRes_GetCard() {
    var send_list = func.funcStorageGet(KEY__hs_send_list);
    if (!send_list) {
        send_list = [];
    }
    console.log(send_list);
    var btn_x, btn_y, title;
    var index = 0; //标记找第几个
    while (true) {
        func.funcSleep(3);

        var do_down = true;
        var btn = className('android.widget.TextView').textStartsWith('预估收益').findOnce(index);
        var pa;//父组件
        if (btn == null) {
            func.funcAlert('找不到可发内容');
            index = 0;
        }
        else {
            pa = btn.parent();
            var title = pa.child(0).text();
            func.funcAlert('判断标题是否已送：' + title); //防止二次发朋友圈
            if (title == '抢免单') {
                //往下滑
            }
            else {
                func.funcSleep(1);
                var is_send = false;
                for (let i = 0; i < send_list.length; i++) {
                    if (title == send_list[i]) {
                        func.funcAlert('找到的内容已发送');
                        func.funcSleep(1);
                        is_send = true;
                        break;
                    }
                }
                if (!is_send) {
                    func.funcAlert('找到新内容');
                    func.funcSleep(2);
                    do_down = false;
                }
                else {
                    if (index < 1) {
                        index++;
                        func.funcAlert('继续找下一个组件：' + index);
                        continue;
                    }
                }
            }
        }

        if (do_down) {
            index = 0;
            func.funcAlert('找到领券-前往下滑');
            func.funcScrollDown();
            continue;
        }

        var bb = pa.bounds();
        btn_x = bb.centerX();
        btn_y = bb.centerY();
        break;
    }
    func.funcAlert('x=' + btn_x + ',y=' + btn_y);
    func.funcSleep(2);
    func.funcAlert('已发送列表添加新标题');
    send_list.push(title);
    func.funcStorageSet(KEY__hs_send_list, send_list);
    func.funcAlert("保存介绍内容：" + title);
    func.funcStorageSet(KEY__next_desc, title);
    func.funcClick(btn_x, btn_y);
}

/**
 * 获取大姐大朋友圈素材
 */
function funcGetBossRes() {
    funcWxQuanSearch();
    func.funcSleep(3);
    //
    func.funcAlert('输入搜索内容');
    setClip('大姐大');
    func.funcSleep(1);
    //
    func.funcClick(125, 160, 3000);
    func.funcClick(150, 279);
    func.funcSleep(1);
    //    
    func.funcAlert('前往【大姐大】朋友圈');
    func.funcClick(226, 396);
    func.funcSleep(1);
    func.funcAlert('前往【大姐大】朋友圈-1');
    func.funcClick(1000, 136);
    func.funcSleep(1);
    func.funcAlert('前往【大姐大】朋友圈-2');
    func.funcClick(119, 359);
    func.funcSleep(1);
    func.funcAlert('前往【大姐大】朋友圈-3');
    func.funcClick(111, 873);
    func.funcSleep(1);
    func.funcAlert('点击【大姐大】最新朋友圈');
    func.funcClick(378, 1267);
    func.funcSleep(1);
    //
    func.funcAlert('前往-复制文字');
    func.funcClick(958, 1845);
    func.funcScrollUp();
    func.funcSleep(2);
    func.funcAlert('复制文字-第一条评论');
    //截图找第一条评论位置
    var quan_img = captureScreen();
    func.funcSleep(1);
    var comment_point = findColor(quan_img, "#eeeeee", {
        region: [20, 1000, 100, 500],
    });
    if (!comment_point) {
        func.funcAlert('没有找到评论');
        return false;
    }
    //复制
    func.funcClick(comment_point.x + 200, comment_point.y + 100, 2000);
    func.funcClick(comment_point.x + 200, comment_point.y + 100 - 260);
    var txt = func.funcGetCp();
    func.funcAlert("剪贴板：" + txt);
    func.funcSleep(2);
    if (txt == '') {
        func.funcAlert('第一条评论内容为空');
        return false;
    }
    func.funcSleep(1);
    //
    func.funcAlert('复制文字-介绍');
    func.funcClick(210, 333, 2000);
    func.funcClick(270, 462);
    func.funcSleep(1);
    var txt_desc = func.funcGetCp();
    func.funcAlert("剪贴板：" + txt_desc);
    func.funcSleep(2);
    //
    func.funcAlert("比较上一次内容是否相同");
    var txt_pre = func.funcStorageGet(KEY__boss_pre);
    if (txt_pre == txt) {
        func.funcAlert("内容相同，无需发送");
        func.funcSleep(2);
        return false;
    }
    func.funcAlert("保存朋友圈内容");
    func.funcStorageSet(KEY__next, txt);
    func.funcStorageSet(KEY__next_desc, txt_desc);
    func.funcSleep(2);
    //
    func.funcAlert("开始保存图片");
    func.funcBack();
    func.funcSleep(1);
    for (let index = 0; index < 3; index++) {
        if (index != 0) {
            func.funcAlert('向左滑动');
            gesture(1000, [800, 1000], [100, 1000])
            func.funcSleep(3);
        }
        //        
        func.funcAlert('保存第' + index + '张图片');
        func.funcClick(500, 1000, 2000);
        func.funcClick(168, 1671);
        func.funcSleep(2);
    }
    //标记发送成功-理论上
    func.funcStorageSet(KEY__boss_pre, txt);

    return true;
}

/**
 * 发朋友圈-图片3张
 */
function funcWxQuanSendImg() {
    var txt = func.funcStorageGet(KEY__next);
    if (!txt) {
        func.funcAlert('没有获取朋友圈素材');
        func.funcSleep(2);
        return;
    }
    var txt_desc = func.funcStorageGet(KEY__next_desc);
    //
    funcWxQuanOpen();
    func.funcSleep(3);
    //
    func.funcAlert('点击发朋友圈按钮');
    func.funcClick(1009, 140);
    //
    func.funcAlert('点击从相册选择');
    func.funcClick(568, 1087);
    func.funcSleep(3);
    func.funcAlert('选择第1张图片');
    func.funcClick(228, 250);
    func.funcAlert('选择第2张图片');
    func.funcClick(490, 250);
    func.funcAlert('选择第3张图片');
    func.funcClick(763, 250);
    func.funcAlert('点击完成按钮');
    func.funcClick(960, 143);
    //
    if (txt_desc != '') {
        func.funcAlert('将介绍放在粘贴板');
        setClip(txt_desc);
        func.funcSleep(1);
        //
        func.funcAlert('粘贴介绍：' + txt_desc);
        func.funcClick(158, 439, 3000);
        func.funcClick(158, 439);
    }
    func.funcSleep(1);
    //
    func.funcAlert('点击发表按钮');
    func.funcClick(960, 143);
    func.funcSleep(3);
    //
    //前往我的朋友圈
    funcWxQuanList();
    func.funcAlert('将素材码变成评论');
    func.funcSleep(1);
    func.funcScrollUp();
    func.funcSleep(3);
    func.funcAlert('点击第一条朋友圈');
    func.funcClick(328, 1388);
    func.funcSleep(1);
    func.funcAlert('点击评论');
    func.funcClick(342, 1837);
    func.funcAlert('将码放在粘贴板');
    func.funcSetCp(txt);
    func.funcSleep(1);
    func.funcAlert('填写评论内容');
    func.funcClick(100, 290, 2000);
    func.funcClick(148, 170);
    func.funcAlert('点击评论发送');
    func.funcClick(946, 141);
    func.funcSleep(1);
    func.funcAlert('清空粘贴板内容');
    func.funcSetCp('');
    func.funcSleep(1);
}

/**
 * 发朋友圈-文字版
 */
function funcWxQuanSend(content) {
    func.funcAlert('将内容放在粘贴板');
    setClip(content);
    func.funcSleep(1);
    //
    func.funcAlert('长按发朋友圈按钮');
    func.funcClick(1009, 140, 2000);
    //
    func.funcAlert('粘贴内容：' + content);
    func.funcClick(158, 439, 3000);
    func.funcClick(158, 439);
    func.funcSleep(1);
    //
    func.funcAlert('点击发表按钮');
    func.funcClick(960, 143);
}

/** 打开花生日记 */
function funcHsOpen() {
    func.funcAlert('清空粘贴板内容');
    setClip('');
    func.funcAlert('打开花生日记');
    func.funcOpendAppPage("com.jf.lkrj/com.jf.lkrj.MainActivity")
}

/** 打开微信朋友圈 */
function funcWxQuanOpen() {
    func.funcAlert('打开朋友圈');
    func.funcOpendAppPage("com.tencent.mm/com.tencent.mm.plugin.sns.ui.SnsTimeLineUI")
}
/** 打开微信搜索 */
function funcWxQuanSearch() {
    func.funcAlert('打开微信搜索');
    func.funcOpendAppPage('com.tencent.mm/com.tencent.mm.plugin.fts.ui.FTSMainUI')
}
/** 打开我的朋友圈列表 */
function funcWxQuanList() {
    funcWxQuanOpen();
    func.funcSleep(3);
    func.funcAlert('点击我的头像');
    func.funcClick(946, 804);
    func.funcSleep(1);
    func.funcAlert('朋友圈简介');
    func.funcClick(128, 616);
    func.funcSleep(1);
}
/** 返回N次 */
function funcBackN(n) {
    for (let i = 1; i <= n; i++) {
        func.funcAlert('共返回' + n + '次：第' + i);
        back();
        sleep(500);
    }
}