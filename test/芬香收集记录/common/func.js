var func = {
    /** 提示 */
    funcAlert: function (txt) {
        txt = txt.toString();
        console.info(txt);
        if (txt.length > 20) {
            txt = txt.substr(0, 20)
        }
        //
        ui.run(() => {
            func.w.txt.setText(txt);
        });
    },
    /** 暂停 */
    funcSleep: function (s) {
        if (s > 2) {
            threads.start(function () {
                var i = s - 1;
                var interval = setInterval(() => {
                    if (i <= 0) {
                        clearInterval(interval);
                        ui.run(() => {
                            func.w.countdown.setText("");
                        });
                    }
                    else {
                        ui.run(() => {
                            func.w.countdown.setText("暂停" + i);
                        });
                    }
                    i--;
                }, 1000);
            });
        }
        //
        sleep(s * 1000);
    },
    /** 返回 */
    funcBack: function () {
        back();
        func.funcSleep(1);
    },
    /** 点击  */
    funcClick: function (x, y, duration) {
        if (!duration) {
            duration = 300;
        }

        press(x, y, duration);
        func.funcSleep(1);
    },
    /** 向上滑 */
    funcScrollUp: function () {
        gesture(500, [500, 500], [500, 1800]);
        func.funcSleep(1);
    },
    /** 向下滑 */
    funcScrollDown: function () {
        gesture(500, [500, 1400], [500, 400]);
        func.funcSleep(1);
    },
    /** 获取存储对象 */
    funcStorageObj() {
        var sdata = storages.create("DATA");
        return sdata;
    },
    /** 设置内容，缓存 */
    funcStorageSet(key, val) {
        console.log('设置存储内容', key, val);
        var sdata = func.funcStorageObj();
        sdata.put(key, val);
        func.funcSleep(1);
    },
    /** 设置内容，缓存 */
    funcStorageGet(key) {
        console.log('获取存储内容', key);
        var sdata = func.funcStorageObj();
        var v = sdata.get(key);
        func.funcSleep(1);

        return v;
    },
    /** 获取复制内容 */
    funcGetCp() {
        var v = getClip();
        func.funcSleep(1);
        //清空粘贴板
        func.funcSetCp('');
        return v;
    },
    /** 设置复制内容 */
    funcSetCp(v) {
        setClip(v);
        func.funcSleep(1);
    },
    /** 打开app指定页面 */
    funcOpendAppPage(activity) {
        shell("am start -n " + activity, true);
    },
}

//悬浮窗，控制脚本开始
var w = floaty.rawWindow(
    <horizontal>
        <text w="*" h="auto" id="countdown" textColor="red" text="" />
        <button style="Widget.AppCompat.Button.Colored" w="50" id="start" text="运行" textSize="11sp" />
        <button style="Widget.AppCompat.Button.Colored" w="50" id="exit" text="退出" textSize="11sp" />
        <text w="*" h="auto" id="txt" textColor="blue" text="" />
    </horizontal>
);
w.setSize(-2, -2);
w.exit.click(function () {
    console.log('close');
    exit();
});
w.start.click(function () {
    console.log("start");
    threads.start(function () {
        main();
    });
});
//防止悬浮窗关闭
setInterval(() => { }, 3000);

func.w = w;

module.exports = func;