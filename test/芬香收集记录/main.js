setScreenMetrics(1080, 1920);

var func = require('./common/func.js');

var pub_list = []; //已经收集的列表ID
//API
// const api_url = "http://192.168.2.88/app/media/api/funcAddMedia?x=x";
const api_url = "http://php.siyoumi.com/app/media/api/funcAddMedia?x=x";
const pub_add_max = 10;
var pub_add_count = 0;

// main();
function main() {
    pub_add_count = 0;
    while (true) {
        if (pub_add_count >= pub_add_max) {
            break;
        }

        funcGet();
        func.funcSleep(3);
        func.funcAlert('往下滑动，已收集：' + pub_add_count);
        func.funcScrollDown();
        func.funcSleep(3);
    }

    func.funcAlert('收集完成');
}

//收集
function funcGet() {
    var list = className("android.widget.ScrollView").findOne()
    for (var i = 0; i < list.childCount(); i++) {
        var child = list.child(i);
        if (child.className() == 'android.widget.ImageView' && child.text().length > 0) {
            var copy_btn = child.findOne(text('复制文字'));
            if (copy_btn != null) {
                var id = copy_btn.sourceNodeId();
                if (pub_list.indexOf(id) >= 0) {
                    func.funcAlert(id + '已收集过');
                    func.funcSleep(1);
                    continue;
                }
                pub_list.push(id);
                //
                copy_btn.click();
                func.funcAlert(id + ':复制文字');
                func.funcSleep(3);
                var txt = getClip();
                //获取图片逻辑
                func.funcAlert('点击分享图片');
                var share_btn = child.findOne(text('分享图片'));
                share_btn.click();
                func.funcSleep(2);
                func.funcAlert('点击保存图片');
                var save_btn = className("android.widget.ImageView").text("保存图片").findOne(2000);
                save_btn.click();
                func.funcSleep(3);
                func.funcAlert('获取最新一张图片');
                var img_path = "/sdcard/芬香" //保存路径
                var img_arr = files.listDir(img_path);
                var new_pic = img_arr[img_arr.length - 1];
                var pic = images.read(img_path + '/' + new_pic);
                var pic_base64 = images.toBase64(pic, "jpg", 100);
                //
                var post_data = {
                    media_plat: 'jd',
                    media_txt: txt,
                    media_pic_base64: pic_base64,
                };
                funcSaveSendContent(post_data);
                pub_add_count++;
            }
        }
    }
}

//api保存发送内容
function funcSaveSendContent(data) {
    func.funcAlert('api保存发送内容');
    console.log('请求URL：' + api_url);
    var res = http.post(api_url, data);
    if (res.statusCode != 200) {
        func.funcAlert('接口报错');
    }
    // console.log('api', res);
    func.funcSleep(2);
    //
    return data;
}