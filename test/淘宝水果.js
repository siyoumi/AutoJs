auto.waitFor();
sleep(2);
setScreenMetrics(1080, 1920);

//悬浮窗，控制脚本开始
var w = floaty.rawWindow(
    <horizontal>
        <button style="Widget.AppCompat.Button.Colored" w="50" id="start" text="运行" textSize="11sp" />
        <button style="Widget.AppCompat.Button.Colored" w="50" id="exit" text="退出" textSize="11sp" />
        <frame layout_gravity="center">
            <text w="*" h="auto" id="txt" textColor="blue" text="" />
        </frame>
    </horizontal>
);
w.setSize(-2, -2);
w.start.click(function () {
    threads.start(function () {
        main();
    });
});
w.exit.click(function () {
    console.log('exit');
    w.close();
    exit();
});

//防止悬浮窗关闭
setInterval(() => { }, 1000);


function main() {
    //判断是否打开淘宝
    if (currentPackage() != 'com.taobao.taobao') {
        launch("com.taobao.taobao");
    }

    if (currentActivity() != "com.taobao.browser.exbrowser.BrowserUpperActivity") {
        alert('请前往天猫农场->免费领水果');
        return;
    }

    funcAlert("开始");
    if (!textContains("去分享").exists()) {
        funcClick(968, 1689);
        funcSleep(2);
    }
    funcAlert('签到');
    click('签到');
    funcSleep(2);
    funcRun();
}

//点击，下拉，等15秒，返回
function funcRun() {
    funcAlert('开始浏览');

    while (click('去浏览')) {
        funcSleep(2);
        funcSwipeDown();
        funcSleep(2);
        funcSwipeDown();
        funcSleep(15);
        funcBack();
        funcSleep(2);
    }

    funcAlert('结束浏览');
}

function funcDone(x, y) {
    funcAlert('funcDone');

    var is_pass = false;
    if (color_str == '#ff929292') {
        is_pass = true;
        funcAlert('已完成1项');
    }

    return is_pass;
}

function funcSleep(s) {
    if (s > 2) {
        funcAlert("停" + s + "秒");
    }
    sleep(s * 1000);
}
/** 提示 */
function funcAlert(txt) {
    toastLog(txt);
    // ui.run(() => {
    //     w.txt.setText(txt);
    // });
}

/** 返回 */
function funcBack() {
    funcAlert("返回");
    back();
}

/** 向下滑 */
function funcSwipeDown() {
    funcAlert("向下滑");
    swipe(500, 1000, 500, 300, 1000);
}

/** 点击 */
function funcClick(x, y) {
    funcAlert("点击");
    // click(x, y);
    press(x, y, 350);
}