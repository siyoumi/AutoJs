//requestScreenCapture(); //截图
auto.waitFor();
setScreenMetrics(1080, 1920);

var func = require('./common/func.js');
// const api_url = "http://192.168.2.88/app/media/api/funcGetMedia?x=x";
const api_url = "http://php.siyoumi.com/app/media/api/funcGetMedia?x=x";
//获取需要发送的群列表
const file_path = "/sdcard/Pictures/autojs/txt/group.txt";
const file_txt = files.read(file_path);
var list_group = file_txt.split('|'); //群列表
// list_group = ['1150116937']

main();
function main() {
    while (true) {
        var send_data = funcGetSendContent();
        if (send_data == null) {
            exit();
        }

        //发送
        if (send_data.errcode == 0) {
            funcSend(send_data);
        }
        else {
            func.funcAlert(send_data.errmsg);
        }
        func.funcSleep(10);
    }
    //测试用
    // var test_data = {
    //     'type': '',
    //     'send_txt': 'test',
    //     'send_pic': '123',
    // };
    // funcSend(test_data);
    exit();
}

//进行发送
function funcSend(data) {
    func.funcAlert('开始群发');
    func.funcSleep(1);
    for (let i = 0; i < list_group.length; i++) {
        let group_id = list_group[i];
        func.funcAlert('打开群：' + group_id);
        //
        app.startActivity({
            data: 'mqqwpa://im/chat?chat_type=group&uin=' + group_id,
        })
        func.funcSleep(3);
        func.funcAlert('设置发送内容');
        id("input").findOne(2000).setText(data.send_txt);
        if (data.send_pic && data.send_pic != '') {
            func.funcAlert('发送内容含有图片');
            func.funcSleep(1);
            func.funcAlert('打开选择图片');
            id("qq_aio_panel_image").findOne(2000).click()
            func.funcSleep(2);
            func.funcAlert('选择图片第1张');
            var list_pic = id("flow_photo_list").findOne(2000);
            var pic = list_pic.child(0);
            var checkbox = pic.child(1);
            checkbox.click();//选中
            func.funcSleep(2);
        }
        func.funcSleep(1);
        id("fun_btn").findOne(2000).click();
        func.funcSleep(1);
        func.funcBack();
        func.funcAlert('后退');
        func.funcSleep(2);
    }
}

//api获取发送内容
function funcGetSendContent() {
    func.funcAlert('获取发送内容');
    console.log('请求URL：' + api_url);
    var res = http.get(api_url);
    if (res.statusCode != 200) {
        func.funcAlert('接口报错');
        return null;
    }

    var data = res.body.json()
    console.log('api', data);
    //
    if (data.send_pic && data.send_pic != '') {
        func.funcAlert('发送内容存在图片素材-进行保存');
        func.funcSleep(1);
        var save_path = "/sdcard/Pictures/1.jpg" //保存路径
        var pic = images.load(data.send_pic);
        images.save(pic, save_path, "jpg", 50);
        func.funcAlert('图片素材保存成功');
        func.funcSleep(2);
    }

    return data;
}