auto.waitFor();
sleep(2);
setScreenMetrics(1080, 1920);
console.log("当前包名：" + currentPackage());
console.log("当前页面名：" + currentActivity());

// //悬浮窗，控制脚本开始
var w = floaty.rawWindow(
    <horizontal>
        <button style="Widget.AppCompat.Button.Colored" w="50" id="start" text="运行" textSize="11sp" />
        <button style="Widget.AppCompat.Button.Colored" w="50" id="exit" text="退出" textSize="11sp" />
        <text w="*" h="auto" id="txt" textColor="blue" text="" />
        <text w="*" h="auto" id="countdown" textColor="black" text="" />
    </horizontal>
);
w.setSize(-2, -2);
w.start.click(function () {
    threads.start(function () {
        main();
    });
});
w.exit.click(function () {
    console.log('exit');
    w.close();
    exit();
});

//防止悬浮窗关闭
setInterval(() => { }, 1000);

// main();
function main() {
    funcReadMsg();
    funcSleep(2);
    funcReadTv();
}

/** 统一停留时间 */
function funcGetS() {
    return 70;
}

/** 阅读文章 */
function funcReadMsg() {
    click(534, 2242);
    funcAlert('点击学习');
    funcSleep(2);
    funcAlert('点击要闻');
    click('要闻');
    funcSleep(2);

    var index = 1; //已经完成多少个
    while (true) {
        if (index > 6) {
            funcAlert('文章-全部完成');
            break;
        }

        funcAlert('文章-已完成：' + index);
        funcSleep(1);

        var btn_play = text("播报").findOnce(index);
        if (btn_play != null) {
            btn_play.parent().click();
            funcSleep(1);
            //收藏
            click(865, 2277);
            //
            threads.start(function () {
                funcSleep(2);
                funcAlert("模拟阅读");
                for (var i = 1; i < funcGetS() / 5; i++) {
                    funcSwipeDown();
                    funcSleep(5);
                }
            });
            funcSleep(funcGetS());
            funcBack();
            index++;
        }
        else {
            funcSwipeDown();
            funcSleep(2);
        }
    }
    funcAlert('文章-全部完成');
}
/** 学习视频 */
function funcReadTv() {
    click(772, 2267);
    funcSleep(1);
    click('学习视频');

    var index = 0; //已经完成多少个
    var bin_index = 0; //按第几个按钮
    var swipe_down_index = 0; //往下滑-次数
    while (true) {
        funcAlert('视频-已完成：' + index);
        funcSleep(1);

        if (index > 6) {
            funcAlert('视频-全部完成');
            break;
        }

        var btn_play = text("新华社").findOnce(bin_index);
        if (btn_play != null) {
            btn_play.parent().click();
            funcAlert('观看视频=' + bin_index);
            funcSleep(funcGetS());
            funcBack();
            index++;
            bin_index++;
            // swipe_down_index = 0;
        }
        else {
            funcSwipeDown();
            funcSleep(2);
            if (swipe_down_index > 3) {
                funcAlert("重置按钮-index")
                bin_index = 0;
                swipe_down_index = 0;
            }
            swipe_down_index++;
        }
    }
}


/** 提示 */
function funcAlert(txt) {
    ui.run(() => {
        w.txt.setText(txt);
    });
}
/** 暂停 */
function funcSleep(s) {
    if (s > 2) {
        threads.start(function () {
            var i = s - 1;
            var interval = setInterval(() => {
                if (i <= 0) {
                    clearInterval(interval);
                    ui.run(() => {
                        w.countdown.setText("");
                    });
                }
                else {
                    ui.run(() => {
                        w.countdown.setText("倒计时" + i);
                    });
                }
                i--;
            }, 1000);
        });
    }
    //
    sleep(s * 1000);
}
/** 返回 */
function funcBack() {
    funcAlert("返回");
    back();
}

/** 向下滑 */
function funcSwipeDown() {
    funcAlert("往下滑");
    swipe(500, 1200, 500, 300, 1000);
}