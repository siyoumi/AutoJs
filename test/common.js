auto.waitFor();
sleep(1);

setScreenMetrics(1080, 1920);


//悬浮窗，控制脚本开始
var w = floaty.rawWindow(
    <horizontal>
        <button w="50" id="start" text="运行" textSize="11sp" />
        <button w="60" id="exit" text="退出" textSize="11sp" />
    </horizontal>
);
w.setSize(-2, -2);
w.start.click(function () {
    threads.start(function () {
        main();
    });
});
w.exit.click(function () {
    console.log('exit');
    w.close();
    exit();
});

//防止悬浮窗关闭
setInterval(() => { }, 1000);


/** 提示 */
function funcAlert(txt) {
    toastLog(txt);
}
/** 暂停 */
function funcSleep(s) {
    if (s > 2) {
        funcAlert("停" + s + "秒");
    }
    sleep(s * 1000);
}
/** 返回 */
function funcBack() {
    funcAlert("返回");
    back();
}